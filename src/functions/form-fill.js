// This function fetches the information from the server on button click and fills the form.

const cf = document.querySelector('input[id="cf"]');
// const frm = document.querySelector('form[class="frm"]');

const cfRegex = /^[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}$/; // RegEx Tax Code used for the form check.

// Identifying HTML elements via ID.
const formName = document.getElementById("name");
const formSurname = document.getElementById("surname");
const formBirthday = document.getElementById("birthday");
const formAddress = document.getElementById("address");
const formState = document.getElementById("state");
const formCountry = document.getElementById("country");

export function formFill() {
  cf.addEventListener("input", function (e) {
    e.preventDefault();
    cf.value = cf.value.toUpperCase(); // Prevents the user from using lowercase characters.

    async function call(e) {
      let response = await fetch(e);
      if (response.ok) {
        const info = await response.json();
        formName.value = `${info[0]?.name}` || "";
        formSurname.value = `${info[0]?.surname}` || "";
        formBirthday.value = `${info[0]?.birth_day}` || "";
        formAddress.value =
          `${info[0]?.address.city}, ${info[0]?.address.street_name}, ${info[0]?.address.street_number}` ||
          "";
        formState.value = `${info[0]?.address.state}` || "";
        formCountry.value = `${info[0]?.address.country}` || "";
      } else {
        console.log("Houston, we have a personnel problem.");
      }
    }

    if (cfRegex.test(cf.value) == true) {
      call(`http://localhost:3000/people?tax_code=${cf.value}`);
    } else {
      formName.value = "";
      formSurname.value = "";
      formBirthday.value = "";
      formAddress.value = "";
      formState.value = "";
      formCountry.value = "";
    }
  });
}
