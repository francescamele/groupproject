/** */
export function addZero(e) {
  if (e < 10) {
    // NON puoi ridichiarare let month, perché ti fa ghosting di
    // quella fuori dall'if, e quindi diventa undefined perché
    // non le hai dato un valore. Devi assegnare un nuovo valore
    // senza ridichiarare il "let"
    e = "0" + e;
  }

  return e;
}
