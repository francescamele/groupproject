import { doc } from "prettier";

const buttons = document.querySelectorAll(".hover");
const rcpColor = document.getElementById("rcp-code-color");
let selectedValue;

function handleCodeColours(className, hfColor, svgColor) {
  $(className).on("click", function () {
    //deleteChanges(".blue", ".green", ".orange", ".red", ".white", this, ".page1");

    $(".wrapper-header, footer").css({
      backgroundColor: hfColor,
    });
    $("svg").css({
      fill: svgColor,
    });
  });

  buttons.forEach((b) => {
    b.addEventListener("click", (e) => {
      if (e.target.value === selectedValue) {
        selectedValue = "";
        e.currentTarget.classList.add("shadow");
        e.currentTarget.classList.remove("color");

        return;
      }

      buttons.forEach((b) => {
        b.classList.add("shadow");
        b.classList.remove("color");
      });

      selectedValue = e.currentTarget.value;
      e.currentTarget.classList.add("color");
      e.currentTarget.classList.remove("shadow");

      const codeColor = document.querySelector(".color > span");

      rcpColor.innerText = codeColor.textContent;
    });
  });
}

handleCodeColours(".blue", "#9AE3F9", "#32C5F3");
handleCodeColours(".green", "#98DD69", "#74BF42");
handleCodeColours(".orange", "#FCB25C", "#F99F1A");
handleCodeColours(".red", "#F66065", "#EE1C23");
handleCodeColours(".white", "#c4c4c4", "#c4c4c4");
