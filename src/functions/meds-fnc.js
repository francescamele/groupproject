export function medsMenu() {
  async function call(e) {
    let response = await fetch(e);

    if (response.ok) {
      const meds = await response.json();
      const ref = document.querySelector('select[name="states[]"]');

      for (let i = 0; i < meds.length; i++) {
        const option = document.createElement("option");
        option.appendChild(document.createTextNode(meds[i].name));
        ref.appendChild(option);
      }

      return;
    }
  }

  call("http://localhost:3000/drugs");
}
