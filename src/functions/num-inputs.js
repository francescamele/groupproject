function stepUpDown(operator, id, state) {
  if (state === "true") {
    $(operator).on("click", function () {
      document.getElementById(id).stepUp(1);
    });
  } else {
    $(operator).on("click", function () {
      document.getElementById(id).stepDown(1);
    });
  }
}

stepUpDown("#height-plus", "height", "true");
stepUpDown("#height-min", "height", "false");
stepUpDown("#weight-plus", "weight", "true");
stepUpDown("#weight-min", "weight", "false");
stepUpDown("#heartrate-plus", "heartrate", "true");
stepUpDown("#heartrate-min", "heartrate", "false");
stepUpDown("#temp-plus", "temp", "true");
stepUpDown("#temp-min", "temp", "false");
stepUpDown("#pressure-plus", "pressure", "true");
stepUpDown("#pressure-min", "pressure", "false");
stepUpDown("#fr-plus", "fr", "true");
stepUpDown("#fr-min", "fr", "false");
stepUpDown("#oxygen-plus", "oxygen", "true");
stepUpDown("#oxygen-min", "oxygen", "false");
