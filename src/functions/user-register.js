// Getting the recap form elements via ID.
const rcpAddress = document.getElementById("rcp-address");
const rcpBirthday = document.getElementById("rcp-birthday");
const rcpCf = document.getElementById("rcp-cf");
const rcpComa = document.getElementById("rcp-coma");
const rcpCountry = document.getElementById("rcp-country");
const rcpFr = document.getElementById("rcp-fr");
const rcpGender = document.getElementById("rcp-gender");
const rcpHeartrate = document.getElementById("rcp-heartrate");
const rcpHeight = document.getElementById("rcp-height");
const rcpName = document.getElementById("rcp-name");
const rcpOxy = document.getElementById("rcp-oxy");
const rcpPressure = document.getElementById("rcp-pressure");
const rcpState = document.getElementById("rcp-state");
const rcpSurname = document.getElementById("rcp-surname");
const rcpTemp = document.getElementById("rcp-temp");
const rcpTreatment = document.getElementById("rcp-treatment");
const rcpWeight = document.getElementById("rcp-weight");
const rcpHospitalcode = document.getElementById("rcp-code-color");

// Simple function to send data to the server.
export async function patientRegister() {
  const print = document.getElementById("print");

  print.addEventListener("click", async function () {
    // Creating an object template to send through the POST method.
    let patient = {
      personal_info: {
        name: `${rcpName.textContent}`,
        surname: `${rcpSurname.textContent}`,
        tax_code: `${rcpCf.textContent}`,
        address: `${rcpAddress.textContent}`,
        birth_day: `${rcpBirthday.textContent}`,
        country: `${rcpCountry.textContent}`,
        gender: `${rcpGender.textContent}`,
        state: `${rcpState.textContent}`,
      },
      coma: `${rcpComa.textContent}`,
      frequency: `${rcpFr.textContent}`,
      heartrate: `${rcpHeartrate.textContent}`,
      height: `${rcpHeight.textContent}`,
      hospital_code: `${rcpHospitalcode.textContent}`,
      oxygen: `${rcpOxy.textContent}`,
      pressure: `${rcpPressure.textContent}`,
      temperature: `${rcpTemp.textContent}`,
      treatment: `${rcpTreatment.textContent}`,
      weight: `${rcpWeight.textContent}`,
    };

    await fetch("http://localhost:3000/patients", {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=utf-8",
      },
      body: JSON.stringify(patient),
    });
  });
}
