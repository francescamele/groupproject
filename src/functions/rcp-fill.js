// Function used to fill the recap in the last page.
// It listens to the click on the "next" button on the second
// page to copy all the elements the operator inserted on the form.

// Getting the "next" button
const nextBtn = document.querySelector("#btn-next2");
const backBtn = document.getElementById("btn-back2");

// Getting the form elements by IDs
const formAddress = document.getElementById("address");
const formAnam = document.getElementById("anamnesis");
const formBirthday = document.getElementById("birthday");
const formCf = document.getElementById("cf");
const formComa = document.getElementById("coma");
const formCountry = document.getElementById("country");
const formFr = document.getElementById("fr");
const formGender = document.querySelector('input[name="radio"]:checked');
const formHeartrate = document.getElementById("heartrate");
const formHeight = document.getElementById("height");
const formName = document.getElementById("name");
const formOxygen = document.getElementById("oxygen");
const formPressure = document.getElementById("pressure");
const formSurname = document.getElementById("surname");
const formState = document.getElementById("state");
const formTemperature = document.getElementById("temp");
const formWeight = document.getElementById("weight");

// Getting the rcp elements by IDs
const rcpAddress = document.getElementById("rcp-address");
const rcpAnam = document.getElementById("rcp-anam");
const rcpBirthday = document.getElementById("rcp-birthday");
const rcpCf = document.getElementById("rcp-cf");
const rcpComa = document.getElementById("rcp-coma");
const rcpCountry = document.getElementById("rcp-country");
const rcpFr = document.getElementById("rcp-fr");
const rcpGender = document.getElementById("rcp-gender");
const rcpHeartrate = document.getElementById("rcp-heartrate");
const rcpHeight = document.getElementById("rcp-height");
const rcpName = document.getElementById("rcp-name");
const rcpOxy = document.getElementById("rcp-oxy");
const rcpPressure = document.getElementById("rcp-pressure");
const rcpState = document.getElementById("rcp-state");
const rcpSurname = document.getElementById("rcp-surname");
const rcpTemp = document.getElementById("rcp-temp");
const rcpTreatment = document.getElementById("rcp-treatment");
const rcpWeight = document.getElementById("rcp-weight");

nextBtn.addEventListener("click", () => {
  rcpAddress.innerHTML = formAddress.value;
  rcpAnam.innerHTML = formAnam.value;
  rcpBirthday.innerHTML = formBirthday.value;
  rcpCf.innerHTML = formCf.value;
  rcpComa.innerHTML = formComa.value;
  rcpCountry.innerHTML = formCountry.value;
  rcpFr.innerHTML = formFr.value;
  rcpGender.innerHTML = $("input:radio[name=radio]:checked").val();
  rcpHeartrate.innerHTML = formHeartrate.value;
  rcpHeight.innerHTML = formHeight.value;
  rcpName.innerHTML = formName.value;
  rcpOxy.innerHTML = formOxygen.value;
  rcpPressure.innerHTML = formPressure.value;
  rcpState.innerHTML = formState.value;
  rcpSurname.innerHTML = formSurname.value;
  rcpTemp.innerHTML = formTemperature.value;
  rcpWeight.innerHTML = formWeight.value;

  const selected = $("#mySelect2").select2("data");

  function selectedMeds(n) {
    for (let i = 0; i < selected.length; i++) {
      rcpTreatment.innerText += `${" " + n[i].text}`;
    }
  }

  selectedMeds(selected);
});

backBtn.addEventListener("click", () => {
  rcpTreatment.innerText = "";
});
