import "./button-colours";
import "./rcp-fill";

const rcpTreatment = document.getElementById("rcp-treatment");

$(".second-page").hide();
$(".final-page").hide();

// First page to second page
$("#btn-next1").on("click", function () {
  $(".first-page").hide();
  $("#btn-next1").hide();
  $(".second-page").show();
  $("#btn-back1").show();
  $("#btn-next2").show();
  $("#first").removeClass("bigger");
  $("#second").addClass("bigger");
});

// Second page to first page
$("#btn-back1").on("click", function () {
  $("#btn-next1").show();
  $("#first").addClass("bigger");
  $("#second").removeClass("bigger");
  $(".first-page").show();
  $(".second-page").hide();
});

// Second page to last page
$("#btn-next2").on("click", function () {
  $("#btn-back1").hide();
  $("#btn-next2").hide();
  $("#second").removeClass("bigger");
  $("#third").addClass("bigger");
  $(".second-page").hide();
  $(".final-page").show();
});

// Last page to second page
$("#btn-back2").on("click", function () {
  $(".second-page").show();
  $("#btn-next2").show();
  $("#btn-back1").show();
  $(".final-page").hide();
  $("#third").removeClass("bigger");
  $("#second").addClass("bigger");
});

// Clicking on Page 1 btn
$("#first").on("click", function () {
  $("#btn-next1").show();
  $("#first").addClass("bigger");
  $("#second").removeClass("bigger");
  $("#third").removeClass("bigger");
  $(".final-page").hide();
  $(".first-page").show();
  $(".second-page").hide();
  rcpTreatment.innerText = "";
});

// Clicking on Page 2 btn
$("#second").on("click", function () {
  $("#btn-back1").show();
  $("#btn-next2").show();
  $("#first").removeClass("bigger");
  $("#second").addClass("bigger");
  $(".final-page").hide();
  $(".first-page").hide();
  $(".second-page").show();
  rcpTreatment.innerText = "";
});

// Click on Page 3 btn
$("#third").on("click", function () {
  $("#first").removeClass("bigger");
  $("#second").removeClass("bigger");
  $("#third").addClass("bigger");
  $(".final-page").show();
  $(".second-page").hide();
  $(".first-page").hide();

  const selected = $("#mySelect2").select2("data");

  function selectedMeds(n) {
    for (let i = 0; i < selected.length; i++) {
      rcpTreatment.innerText += `${" " + n[i].text}`;
    }
  }

  selectedMeds(selected);
});

// Clear input and recap fields and go to first page
$("#home").on("click", () => {
  $("#btn-next1").show();
  $("#first").addClass("bigger");
  $("#mySelect2").val(null).trigger("change");
  $("#third").removeClass("bigger");
  $(".final-page").hide();
  $(".first-page").show();

  rcpTreatment.innerText = "";
});
