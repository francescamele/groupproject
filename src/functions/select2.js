//javascript function select"

$(document).on("ready", function () {
  $(".js-example-basic-single").select2();
});

$(".js-example-responsive").select2({
  width: "resolve", // need to override the changed default
});

$(".js-example-tags").select2({
  tags: true,
});

$("select").select2({
  placeholder: {
    //id: '-1', // the value of the option
    text: "Select an option:",
  },
});
