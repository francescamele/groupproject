import "core-js/stable";
import "regenerator-runtime/runtime";

// Import funcions
import { addZero } from "./functions/date-time";
import { formFill } from "./functions/form-fill";
import { medsMenu } from "./functions/meds-fnc";
import { patientRegister } from "./functions/user-register";
import "./functions/button-colours";
import "./functions/num-inputs";
import "./functions/opacity-form";
import "./functions/print";
import "./functions/rcp-fill";
import "./functions/select2";
import "./functions/menu";

// Get the date and time for the header
const dateSpan = document.getElementById("date-span");
const timeSpan = document.getElementById("time-span");
const today = new Date();

let day = today.getDate();
let hours = today.getHours();
let minutes = today.getMinutes();
let month = today.getMonth() + 1;
let year = today.getFullYear();

day = addZero(day);
hours = addZero(hours);
minutes = addZero(minutes);
month = addZero(month);
year = addZero(year);

let date = day + "/" + month + "/" + year;
let time = hours + ":" + minutes;

dateSpan.innerHTML = date;
timeSpan.innerHTML = time;

// Call the imported functions to fill the form and the select
formFill();
medsMenu();
patientRegister();
