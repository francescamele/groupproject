/**
 * 17.01 REC II 
 * Correzione appunti
 * 
 */
// C'è un div di classe bot, con dentro un bottone
const buttons = document.querySelectorAll('div.buttons > hover');

/**
 * Holds the value of the selected button
 * 
 * @var {string}
 */
let selected;

buttons.forEach(b => {
    b.addEventListener('click', e => {
        // Deselezionare un bottone selezionato:
        if (e.currentTarget.value === selected) {
            // Per evitare che lo faccia una sola volta: svuoto selected 
            // ...19' così da far capire a js che non è più selezionato. 
            // Rimane selezionato, se non svuoto il selected 
            selected = '';
            // currentTarget: vedi appunti e recII 50'~
            e.currentTarget.classList.remove(selected);

            return;
        }

        // Se seleziono un altro bottone, questa riga va bene: ...(8')

        // Questo leva la classe selected a tutti gli el, così da togliere 
        // l'ombra dell'el selezionato
        buttons.forEach(b => b.classList.remove('selected'));

        selected = e.currentTarget.value;
        e.currentTarget.classList.add('selected');
    });
});



/**
 * PAG CODICE PRIORITA'
 * 31'
 * 
 * Aggiungi classe .bot ai bottoni col codice priorità
 * Nello script, scriviamo: 
 *      dist/index.bundle.js
 *      dist/secondoEntryPoint.bundle.js
 * 
 * Al button aggiungo il value per avere una discriminante.
 */

/**
 * PRENDERE PAZIENTI
 * 1H01
 */

/**
 * Returns a Patient model accordingly to the given data
 * 
 * @param {object} data An obj as returned by the /people endpoint
 */
function objectModel(data) {
    return new Patient(
        data.id,
        data.name,
        data.surname,
        data.taxCode,
        '',
        '',
        data.birth_day,
        data.address.city,
        data.address.country,
        data.address.state,
        data.address.streetName,
        data.address.streetNumber
    );
}

export const getPatients = async taxCode => {
    await fetch(`http://localhost:3000/people?tax_code=${taxCode}`);
    //La funzione la faccio asincrona così poi posso usare await fetch
    const patients = await response.json();

    return patients.map(obj => objToModel(obj));
}

// 1h17 II
export const updatePatient = async patient => {
    const response = await fetch(`http://localhost:3000/people/${patient.id}`, {
        body: JSON.stringify({

        }),
        method: 'PUT'
    });

    const data = await response.json();

    return objToModel(data);
    // Oppure:
    return objToModel(await response.json());
}



export class Patient {
    // this...
    constructor(
        id, 
        name, 
        surname, 
        taxCode, 
        birth_day, 
        city, 
        country, 
        state, 
        streetName, 
        streetNumber, 
        bloodGroup,
        rh
    )
}

// Per prendermi users dopo aver cambiato il CF:
let patient = null;
const taxCodeInput = document.querySelector('input[name="taxCode"]');

taxCodeInput.addEventListener('change', e => {
    // Gli passo il valore all'interno del campo in input che ha scatenato 
    // quest'evento change
    // 1h12 ...
    const patient = await getPatients(e.target.value);
    // getPatients ci dà un array: mi prendo il primo el
    const patient = patients[0];

    if (!patient) {
        alert('Nessun paziente trovato!');
        return;
    }

    // Altrimenti compilo il form coi valori contenuti
    /**
     * Visto che per la call in post I need to save info on patient esistente, 
     * invece di tener traccia di tutto il model con info con scope strani ? 1h15,
     * creo un input field con type="hidden" in fondo al form: quando ricevo 
     * info dal backend, mi prelevo le info su quel campo
     */
    const idInput = document.querySelector('input[name="id"]');
    idInput.value = patient.id;
    // 1h16 ...

})

// print recup

$("#print").on("click", function () {
    print(".finalP");
  });
  